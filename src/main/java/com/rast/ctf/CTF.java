package com.rast.ctf;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameInstanceManager;
import com.rast.gamecore.util.Kits;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

public class CTF extends JavaPlugin {

    private static CTF plugin; // this plugin
    private static Settings settings; // the settings for this game
    private static CTFGame ctfGame; // the game object that gets registered wth GameCore
    private static File templateFolder; // the folder for the maps
    private static PlayerTags playerTags; // the player tags
    private static GameInstanceManager instanceManager; // the instance manager
    private static Kits kits;

    // the plugin getter
    public static CTF getPlugin() {
        return plugin;
    }

    // the settings getter
    public static Settings getSettings() {
        return settings;
    }

    // the getter for the plugin's game. the name of this should be changed to (Plugin Name + "Game")
    public static CTFGame getCTFGame() {
        return ctfGame;
    }

    // the template folder getter
    public static File getTemplateFolder() {
        return templateFolder;
    }

    // the player tags getter
    public static PlayerTags getPlayerTags() {
        return playerTags;
    }

    // the instance manager
    public static GameInstanceManager getInstanceManager() {
        return instanceManager;
    }

    public static Kits getKits() {
        return kits;
    }

    @Override
    public void onEnable() {
        plugin = this; // get this plugin instance
        settings = new Settings(); // load the settings
        playerTags = new PlayerTags(); // create a player tags instance

        // Setup the map template folder
        templateFolder = new File(plugin.getDataFolder().getAbsoluteFile() + "/maps/");
        if (!templateFolder.exists()) {
            if (!templateFolder.mkdirs()) {
                getLogger().warning("was unable to create the path (" + templateFolder.getAbsolutePath() + ")");
            }
        }

        ctfGame = new CTFGame("CTF", Arrays.asList(Objects.requireNonNull(templateFolder.list())), false, this); // create a new game instance which should only be made once
        ctfGame.addMapConfigs(settings.getMapConfigs());
        instanceManager = new GameInstanceManager(ctfGame); // create the instance manager
        kits = new Kits(this, ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Kit Menu", Material.CHEST, false,
                ChatColor.YELLOW + "" + ChatColor.BOLD + "Kit Menu");

        GameCore.getGameMaster().registerGame(ctfGame); // register the game with GameCore
        GameCore.getKitsManager().registerKits(ctfGame, kits);

        // create a player group for the game
        // player groups are used by GameCore to determine who can see what chat
        GameCore.getGameMaster().createPlayerGroup(ctfGame.getName());

        // register events
        getServer().getPluginManager().registerEvents(new Events(), this);
        Objects.requireNonNull(plugin.getCommand("ctf")).setExecutor(new Commands());

    }

    @Override
    public void onDisable() {
        instanceManager.purgeInstances();
    }
}
