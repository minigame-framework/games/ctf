package com.rast.ctf;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameInstance;
import com.rast.gamecore.GameStatus;
import com.rast.gamecore.GameWorld;
import com.rast.gamecore.scores.ScoreFunction;
import com.rast.gamecore.scores.ScoreManager;
import com.rast.gamecore.util.BroadcastWorld;
import com.rast.gamecore.util.CleanPlayer;
import com.rast.gamecore.util.MatchMaker;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.*;

import java.util.*;

public class CTFInstance extends GameInstance {

    // variables
    private final int maxPlayers;
    private final int minPlayers;
    private final Location flag1Loc;
    private final Location flag2Loc;
    private final BlockData flag1;
    private final BlockData flag2;
    private final Block flag1base;
    private final Block flag2base;
    private final int reqFlags;

    // bossbars
    private final BossBar bossBar;

    // Player groups
    private final Set<Player> waitingPlayers = new HashSet<>();
    private final Set<Player> gamePlayers = new HashSet<>();
    private final Set<Player> respawnPlayers = new HashSet<>();
    private final Set<Player> deadPlayers = new HashSet<>();

    // game flags
    private boolean gameStarted = false;
    private boolean gameEnded = false;
    private boolean pvpEnabled = false;
    private boolean team1Capturing = false;
    private boolean team2Capturing = false;

    // teams
    private final Set<Player> team1players = new HashSet<>();
    private final Set<Player> team2players = new HashSet<>();

    // scoreboard for team1
    private final Scoreboard sb1 = Objects.requireNonNull(Bukkit.getScoreboardManager()).getNewScoreboard();
    private final Objective aheaderObjective = sb1.registerNewObjective("aheader", "dummy", ChatColor.RED + ChatColor.BOLD.toString() + "Capture The Flag  ");
    private Score aprevTeam1Flags;
    private Score aprevTeam2Flags;
    private final Team ateam1 = sb1.registerNewTeam("Team1");
    private final Team ateam2 = sb1.registerNewTeam("Team2");
    // scoreboard for team2
    private final Scoreboard sb2 = Objects.requireNonNull(Bukkit.getScoreboardManager()).getNewScoreboard();
    private final Objective bheaderObjective = sb2.registerNewObjective("bheader", "dummy", ChatColor.RED + ChatColor.BOLD.toString() + "Capture The Flag  ");
    private Score bprevTeam1Flags;
    private Score bprevTeam2Flags;
    private final Team bteam1 = sb2.registerNewTeam("Team1");
    private final Team bteam2 = sb2.registerNewTeam("Team2");

    // default
    private final Scoreboard defaultSB = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard();

    // team flag count
    private int team1FlagCount = 0;
    private int team2FlagCount = 0;

    // flag players
    private Player team1Capturer;
    private Player team2Capturer;

    // start timer and grace timer settings
    private long startTimerCount = 0;
    private BukkitTask startCounter;

    // respawn timers
    private final HashMap<Player, RespawnTimer> respawnTimers = new HashMap<>();

    // score tallies
    private final HashMap<Player, Integer> playerKills = new HashMap<>();
    private final HashMap<Player, Integer> playerDeaths = new HashMap<>();

    // despawn timer object
    private BukkitTask worldDespawnTimer;

    // get the variables for the game
    public CTFInstance(GameWorld gameWorld) {
        super(gameWorld);
        maxPlayers = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getMaxPlayers();
        minPlayers = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getMinPlayers();
        reqFlags = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getReqFlags();
        bossBar = Bukkit.createBossBar(ChatColor.YELLOW + "Waiting for players...", BarColor.YELLOW, BarStyle.SOLID);
        bossBar.setProgress(1.0);
        // scoreboard stuff
        ChatColor team1color = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getTeam1color();
        ChatColor team2color = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getTeam2color();
        // team1
        flag1Loc = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getTeam1flag();
        flag1Loc.setWorld(getGameWorld().getBukkitWorld());
        flag1 = flag1Loc.getBlock().getBlockData().clone();
        flag1base = flag1Loc.getBlock().getRelative(BlockFace.DOWN);
        aheaderObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
        Score aspacer1 = aheaderObjective.getScore(ChatColor.GRAY + " -----------------");
        aspacer1.setScore(9);
        Score aspacer2 = aheaderObjective.getScore(ChatColor.GRAY + "                 ");
        aspacer2.setScore(8);
        Score ateam1name = aheaderObjective.getScore(team1color + " | Team 1 | " + ChatColor.GREEN + "← You");
        ateam1name.setScore(7);
        Score ateam2name = aheaderObjective.getScore(team2color + " | Team 2 |");
        ateam2name.setScore(4);
        Score aspacer3 = aheaderObjective.getScore(ChatColor.GRAY + "                  ");
        aspacer3.setScore(2);
        Score aspacer4 = aheaderObjective.getScore(ChatColor.GRAY + " ----------------- ");
        aspacer4.setScore(1);
        Score aspacer5 = aheaderObjective.getScore(ChatColor.GRAY + "             ");
        aspacer5.setScore(5);
        ateam1.setColor(team1color);
        ateam2.setColor(team2color);
        // team2
        flag2Loc = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getTeam2flag();
        flag2Loc.setWorld(getGameWorld().getBukkitWorld());
        flag2 = flag2Loc.getBlock().getBlockData().clone();
        flag2base = flag2Loc.getBlock().getRelative(BlockFace.DOWN);
        bheaderObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
        Score bspacer1 = bheaderObjective.getScore(ChatColor.GRAY + " -----------------");
        bspacer1.setScore(9);
        Score bspacer2 = bheaderObjective.getScore(ChatColor.GRAY + "                 ");
        bspacer2.setScore(8);
        Score bteam1name = bheaderObjective.getScore(team1color + " | Team 1 |");
        bteam1name.setScore(7);
        Score bteam2name = bheaderObjective.getScore(team2color + " | Team 2 | " + ChatColor.GREEN + "← You");
        bteam2name.setScore(4);
        Score bspacer3 = bheaderObjective.getScore(ChatColor.GRAY + "                  ");
        bspacer3.setScore(2);
        Score bspacer4 = bheaderObjective.getScore(ChatColor.GRAY + " ----------------- ");
        bspacer4.setScore(1);
        Score bspacer5 = bheaderObjective.getScore(ChatColor.GRAY + "             ");
        bspacer5.setScore(5);
        bteam1.setColor(team1color);
        bteam2.setColor(team2color);
        // for all scoreboards
        team1flags(0, 0, 6);
        team2flags(0, 0, 3);
    }

    // add player
    public void addPlayer(Player player) {
        // first ensure that the game is waiting open and the match does not have the player
        if (getGameWorld().getStatus() == GameStatus.WAITING_OPEN && !hasPlayer(player)) {
            // send the player to the game
            waitPlayerPrep(player);
            bossBar.addPlayer(player);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName()
                    + " has joined the match. " + ChatColor.DARK_GRAY + '(' + getPlayerCount() + '/' + maxPlayers + ')');
            startCountdown();
        }
        worldStatusRefresh();
    }

    public void removePlayer(Player player) {
        if (hasPlayer(player)) {
            // remove the tags that may have been assigned to the player
            waitingPlayers.remove(player);
            gamePlayers.remove(player);
            deadPlayers.remove(player);
            respawnPlayers.remove(player);
            bossBar.removePlayer(player);
            player.setScoreboard(defaultSB);
            removePlayerTeams(player);
            dropFlag(player);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName() + " has left the match.");
            if (!isGameReady()) {
                stopStartCountdown();
            }
            gameWinCheck();
            CTF.getKits().removePlayerKit(player);
            worldStatusRefresh();
            saveScoreTallies(player);
            if (respawnTimers.containsKey(player)) {
                respawnTimers.remove(player).stopTask();
            }
        }
    }

    // prep the player for waiting
    private void waitPlayerPrep(Player player) {
        gamePlayers.remove(player);
        deadPlayers.remove(player);
        waitingPlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.ADVENTURE);
        Location loc = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        player.getInventory().addItem(CTF.getKits().getKitMenuItem());
    }

    // prep the player for the game
    private void gamePlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        gamePlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SURVIVAL);
        Location loc;
        if (team1players.contains(player)) {
            loc = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getTeam1spawn();
        } else if (team2players.contains(player)) {
            loc = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getTeam2spawn();
        } else {
             loc = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        }
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        CTF.getKits().equipKit(player);
        gameWinCheck();
    }

    // prep the player for the game
    private void respawnPlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        gamePlayers.remove(player);
        respawnPlayers.add(player);
        player.setGameMode(GameMode.SPECTATOR);
        Location loc = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        respawnTimers.put(player, new RespawnTimer(player, 5, () -> {
            if (!gameEnded) {
                gamePlayerPrep(player);
            }
        }));
    }

    // prep the player for spectate
    private void spectatePlayerPrep(Player player) {
        gamePlayers.remove(player);
        waitingPlayers.remove(player);
        deadPlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SPECTATOR);
        Location loc = CTF.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
    }

    // kill a player
    public void killPlayer(Player player, Player killer) {
        dropFlag(player);
        playerDeaths.compute(player, (k, v) -> (v == null) ? 1 : v+1);
        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
        if (killer != null) {
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + " has been killed by " + killer.getName() + '!');
            playerKills.compute(killer, (k, v) -> (v == null) ? 1 : v + 1);
        } else {
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + " has been killed!");
        }
        respawnPlayerPrep(player);
    }

    // check to see if this map has a player
    public boolean hasPlayer(Player player) {
        return waitingPlayers.contains(player) || gamePlayers.contains(player) || deadPlayers.contains(player) || respawnPlayers.contains(player);
    }

    public int getPlayerCount() {
        return waitingPlayers.size() + deadPlayers.size() + gamePlayers.size() + respawnPlayers.size();
    }

    private boolean isGameReady() {
        return minPlayers <= waitingPlayers.size();
    }

    private void startCountdown() {
        // end if game has started or the game is not ready
        if (!isGameReady() || gameStarted) {
            return;
        }
        // do not start the counter if the counter is already running
        if (startCounter != null && !startCounter.isCancelled()) {
            return;
        }
        // we can finally start the counter
        startTimerCount = CTF.getSettings().getGameCountdown();
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "There are enough players to start the match.");
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
        startCounter = Bukkit.getScheduler().runTaskTimer(CTF.getPlugin(), () -> {
            if (startTimerCount == 0 && isGameReady()) {
                // timer has reached 0, it is show time
                gameStarted = true;
                worldStatusRefresh();
                bossBar.setVisible(false);
                placePlayersOnTeams();
                // move players into the arena
                Set<Player> playersToMove = new HashSet<>(getWaitingPlayers());
                for (Player playerToMove : playersToMove) {
                    gamePlayerPrep(playerToMove);
                }
                startGame();
                stopStartCountdown();
                return;
            } else if (startTimerCount == 0) {
                // if the game was not ready and the count is at 0 we want to cancel
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                bossBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
                bossBar.setProgress(1.0);
                stopStartCountdown();
                return;
            }
            if (startTimerCount == 1) {
                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " second...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " second.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);
            } else if (startTimerCount <= 10) {
                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " seconds.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);
            } else {
                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");
            }
            bossBar.setProgress((double) startTimerCount / 30);
            if (waitingPlayers.size() >= maxPlayers && startTimerCount > 10) {
                startTimerCount = CTF.getSettings().getGameCountdownFast();
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game full! Starting match in " + startTimerCount + 's');
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
            } else {
                startTimerCount--;
            }
        }, 0, 20);
    }

    // stop the starter countdown
    private void stopStartCountdown() {
        if (startCounter != null && !startCounter.isCancelled()) {
            startCounter.cancel();
            if (!isGameReady() && !gameStarted) {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
            }
            bossBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
            bossBar.setProgress(1.0);
        }
    }

    // the countdown at the end of the game until the game closes
    private void endGameCountdown() {
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.BLUE + "Match is closing in 10 seconds. Use /leave to go back to spawn.");
        if (!CTF.getPlugin().isEnabled()) {
            return;
        }
        Bukkit.getScheduler().runTaskLater(CTF.getPlugin(), () -> CTF.getInstanceManager().purgeInstance(this), 20 * 10L);
    }

    // start the game
    private void startGame() {
        pvpEnabled = true;
    }

    // save player score tallies
    private void saveScoreTallies(Player player) {
        ScoreManager sm = GameCore.getScoreManager();
        if (playerKills.get(player) != null) {
            sm.modifyScore(player, CTF.getCTFGame(), "Kills", playerKills.remove(player), ScoreFunction.ADD);
        }
        if (playerDeaths.get(player) != null) {
            sm.modifyScore(player, CTF.getCTFGame(), "Deaths", playerDeaths.remove(player), ScoreFunction.ADD);
        }
    }

    // check if the win condition has been met. If so end the game.
    private void gameWinCheck() {
        if (!gameStarted) {
            return;
        }
        if (team1players.isEmpty() || team2players.isEmpty()) {
            if (!gameEnded) {
                gameEnded = true;
                if (team2players.isEmpty()) {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + "Team 1 has won the match!");
                    for (Player player : team1players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }

                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, CTF.getCTFGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, CTF.getCTFGame(), "Wins").get());
                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null) ? 0 : playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null) ? 0 : playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                    for (Player player : team2players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null) ? 0 : playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null) ? 0 : playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                } else {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + "Team 2 has won the match!");
                    for (Player player : team2players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, CTF.getCTFGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, CTF.getCTFGame(), "Wins").get());
                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null) ? 0 : playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null) ? 0 : playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                    for (Player player : team1players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null) ? 0 : playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null) ? 0 : playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                }
                endGameCountdown();
                return;
            }
        }

        if (team1FlagCount >= reqFlags || team2FlagCount >= reqFlags) {
            if (!gameEnded) {
                gameEnded = true;
                if (team1FlagCount == team2FlagCount) {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "" + ChatColor.BOLD + "Nobody won the match.");
                } else if (team1FlagCount > team2FlagCount){
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + "Team 1 has won the match!");
                    for (Player player : team1players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }

                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, CTF.getCTFGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, CTF.getCTFGame(), "Wins").get());
                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                    for (Player player : team2players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                } else {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + "Team 2 has won the match!");
                    for (Player player : team2players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }

                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, CTF.getCTFGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, CTF.getCTFGame(), "Wins").get());
                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                    for (Player player : team1players) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                }
                endGameCountdown();
            }
        }
    }

    // refresh the gameWorld status
    private void worldStatusRefresh() {
        if (gameStarted) {
            getGameWorld().setStatus(GameStatus.RUNNING_CLOSED);
            return;
        }

        if (getPlayerCount() >= maxPlayers) {
            getGameWorld().setStatus(GameStatus.WAITING_CLOSED);
        } else {
            getGameWorld().setStatus(GameStatus.WAITING_OPEN);
        }
        getGameWorld().setPlayerCount(getPlayerCount());
        worldDespawnTimer();
    }

    // start the world despawn time if conditions are met
    private void worldDespawnTimer() {
        if (!CTF.getPlugin().isEnabled()) {
            return;
        }
        if (worldDespawnTimer == null || worldDespawnTimer.isCancelled()) {
            if (getPlayerCount() == 0) {
                worldDespawnTimer = Bukkit.getScheduler().runTaskLater(CTF.getPlugin(), () -> {
                    if (getPlayerCount() == 0) {
                        CTF.getInstanceManager().purgeInstance(this);
                    }
                }, 20 * CTF.getSettings().getWorldDespawnTime());
            }
        } else if (worldDespawnTimer != null && !worldDespawnTimer.isCancelled() && getPlayerCount() != 0) {
            worldDespawnTimer.cancel();
        }
    }

    // getters
    public Set<Player> getWaitingPlayers() {
        return waitingPlayers;
    }

    public Set<Player> getGamePlayers() {
        return gamePlayers;
    }

    public Set<Player> getDeadPlayers() {
        return deadPlayers;
    }

    public Set<Player> getTeam1players() {
        return team1players;
    }

    public Set<Player> getTeam2players() {
        return team2players;
    }

    public boolean pvpEnabled() {
        return pvpEnabled;
    }

    // scoreboard score getters
    public void team1flags(int flags, int capturing, int index) {
        if (aprevTeam1Flags != null) {
            sb1.resetScores(aprevTeam1Flags.getEntry());
        }
        if (bprevTeam1Flags != null) {
            sb2.resetScores(bprevTeam1Flags.getEntry());
        }

        StringBuilder sb = new StringBuilder(ChatColor.GRAY  + " Flags:" + CTF.getSettings().getMapConfig(getGameWorld().getMap()).getTeam2color());

        for (int i = 0; i < reqFlags; i++) {
            if (i+1 <= flags) {
                sb.append(" ⬛");
            } else if (i+1 <= flags+capturing){
                sb.append(" ▒");
            } else {
                sb.append(" ⬜");
            }
        }
        aprevTeam1Flags = aheaderObjective.getScore(sb.toString());
        aprevTeam1Flags.setScore(index);
        bprevTeam1Flags = bheaderObjective.getScore(sb.toString());
        bprevTeam1Flags.setScore(index);
    }
    // scoreboard score getters
    public void team2flags(int flags, int capturing, int index) {
        if (aprevTeam2Flags != null) {
            sb1.resetScores(aprevTeam2Flags.getEntry());
        }
        if (bprevTeam2Flags != null) {
            sb2.resetScores(bprevTeam2Flags.getEntry());
        }

        StringBuilder sb = new StringBuilder(ChatColor.GRAY  + " Flags:" + CTF.getSettings().getMapConfig(getGameWorld().getMap()).getTeam1color());

        for (int i = 0; i < reqFlags; i++) {
            if (i+1 <= flags) {
                sb.append(" ⬛");
            } else if (i+1 <= flags+capturing){
                sb.append(" ▒");
            } else {
                sb.append(" ⬜");
            }
        }
        aprevTeam2Flags = aheaderObjective.getScore(sb.toString());
        aprevTeam2Flags.setScore(index);
        bprevTeam2Flags = bheaderObjective.getScore(sb.toString());
        bprevTeam2Flags.setScore(index);
    }

    // add player to team1
    public void addPlayerTeam1(Player player) {
        team1players.add(player);
        ateam1.addEntry(player.getName());
        bteam1.addEntry(player.getName());
        player.setScoreboard(sb1);
    }

    // add player to team2
    public void addPlayerTeam2(Player player) {
        team2players.add(player);
        ateam2.addEntry(player.getName());
        bteam2.addEntry(player.getName());
        player.setScoreboard(sb2);
    }

    // remove player from all teams
    public void removePlayerTeams(Player player) {
        team1players.remove(player);
        team2players.remove(player);
        ateam1.removeEntry(player.getName());
        ateam2.removeEntry(player.getName());
        bteam1.removeEntry(player.getName());
        bteam2.removeEntry(player.getName());
    }

    // player team chooser
    public void placePlayersOnTeams() {
        // generate the teams
        List<List<Player>> teams = MatchMaker.placePlayersOnTeams(new ArrayList<>(waitingPlayers), 2);

        // now place the players on the teams
        for (Player player : teams.get(0)) {
            addPlayerTeam1(player);
        }
        for (Player player : teams.get(1)) {
            addPlayerTeam2(player);
        }
    }

    // have a player pick up the flag
    private void pickUpFlag(Player player, Block block) {
        if (gameStarted && gamePlayers.contains(player)) {
            if (team1players.contains(player)) {
                if (!team1Capturing) {
                    if (block.getLocation().getBlockX() == flag2Loc.getBlockX() &&
                            block.getLocation().getBlockY() == flag2Loc.getBlockY() &&
                            block.getLocation().getBlockZ() == flag2Loc.getBlockZ()) {
                        team1Capturing = true;
                        team1Capturer = player;
                        team1flags(team1FlagCount, 1, 6);
                        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_WOLF_HURT, 1.8f);
                        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.YELLOW + player.getName() + " has picked up " + ateam2.getColor() + "Team 2" + ChatColor.YELLOW + "'s flag!");
                        flag2Loc.getBlock().setType(Material.AIR);
                        player.getInventory().setHelmet(new ItemStack(flag2.getMaterial()));
                        player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Right click your flag's base to return the flag.");
                    }
                }
            }
            if (team2players.contains(player)) {
                if (!team2Capturing) {
                    if (block.getLocation().getBlockX() == flag1Loc.getBlockX() &&
                            block.getLocation().getBlockY() == flag1Loc.getBlockY() &&
                            block.getLocation().getBlockZ() == flag1Loc.getBlockZ()) {
                        team2Capturing = true;
                        team2Capturer = player;
                        team2flags(team2FlagCount, 1, 3);
                        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_WOLF_HURT, 1.8f);
                        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.YELLOW + player.getName() + " has picked up " + ateam1.getColor() + "Team 1" + ChatColor.YELLOW + "'s flag!");
                        flag1Loc.getBlock().setType(Material.AIR);
                        player.getInventory().setHelmet(new ItemStack(flag1.getMaterial()));
                        player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Right click your flag's base to return the flag.");
                    }
                }
            }
        }
    }

    // have a player capture the flag
    private void captureFlag(Player player, Block block) {
        if (gameStarted && !gameEnded) {
            if (team1Capturing) {
                if (player.equals(team1Capturer)) {
                    if (block.getLocation().getBlockX() == flag1base.getLocation().getBlockX() &&
                            block.getLocation().getBlockY() == flag1base.getLocation().getBlockY() &&
                            block.getLocation().getBlockZ() == flag1base.getLocation().getBlockZ()) {
                        team1Capturing = false;
                        team1Capturer = null;
                        team1FlagCount++;
                        team1flags(team1FlagCount, 0, 6);
                        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_WOLF_HOWL, 2f);
                        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.YELLOW + player.getName() + " has captured " + ateam2.getColor() + "Team 2" + ChatColor.YELLOW + "'s flag!");
                        flag2Loc.getBlock().setBlockData(flag2);
                        player.getInventory().setHelmet(new ItemStack(Material.AIR));
                        gameWinCheck();
                    }
                }
            }
            if (team2Capturing) {
                if (player.equals(team2Capturer)) {
                    if (block.getLocation().getBlockX() == flag2base.getLocation().getBlockX() &&
                            block.getLocation().getBlockY() == flag2base.getLocation().getBlockY() &&
                            block.getLocation().getBlockZ() == flag2base.getLocation().getBlockZ()) {
                        team2Capturing = false;
                        team2Capturer = null;
                        team2FlagCount++;
                        team2flags(team2FlagCount, 0, 3);
                        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_WOLF_HOWL, 2f);
                        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.YELLOW + player.getName() + " has captured " + ateam1.getColor() + "Team 1" + ChatColor.YELLOW + "'s flag!");
                        flag1Loc.getBlock().setBlockData(flag1);
                        player.getInventory().setHelmet(new ItemStack(Material.AIR));
                        gameWinCheck();
                    }
                }
            }
        }
    }

    // have a player capture the flag
    private void dropFlag(Player player) {
        if (gameStarted && !gameEnded) {
            if (team1Capturing) {
                if (player.equals(team1Capturer)) {
                    if (team1Capturer.equals(player)) {
                        team1Capturing = false;
                        team1Capturer = null;
                        team1flags(team1FlagCount, 0, 6);
                        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_WOLF_GROWL, 2f);
                        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.YELLOW + player.getName() + " has dropped " + ateam2.getColor() + "Team 2" + ChatColor.YELLOW + "'s flag!");
                        flag2Loc.getBlock().setBlockData(flag2);
                        player.getInventory().setHelmet(new ItemStack(Material.AIR));
                        gameWinCheck();
                    }
                }
            }
            if (team2Capturing) {
                if (player.equals(team2Capturer)) {
                    if (team2Capturer.equals(player)) {
                        team2Capturing = false;
                        team2Capturer = null;
                        team2flags(team1FlagCount, 0, 3);
                        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_WOLF_GROWL, 2f);
                        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.YELLOW + player.getName() + " has dropped " + ateam1.getColor() + "Team 1" + ChatColor.YELLOW + "'s flag!");
                        flag1Loc.getBlock().setBlockData(flag1);
                        player.getInventory().setHelmet(new ItemStack(Material.AIR));
                        gameWinCheck();
                    }
                }
            }
        }
    }

    // call the flag capturing methods
    public void captureAction(Player player, Block block) {
        pickUpFlag(player, block);
        captureFlag(player, block);
    }

    public boolean sameTeam(Player player1, Player player2) {
        if (team1players.contains(player1) && team1players.contains(player2)) {
            return true;
        }
        return team2players.contains(player1) && team2players.contains(player2);
    }
}
