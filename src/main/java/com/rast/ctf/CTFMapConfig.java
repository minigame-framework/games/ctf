package com.rast.ctf;

import com.rast.gamecore.MapConfig;
import org.bukkit.ChatColor;
import org.bukkit.Location;

public class CTFMapConfig extends MapConfig {
    private final int minPlayers;
    private final int reqFlags;
    private final Location team1spawn, team2spawn, team1flag, team2flag;
    private final ChatColor team1color, team2color;

    public CTFMapConfig(String name, Location mainSpawn, Location team1spawn, Location team2spawn, Location team1flag, Location team2flag, ChatColor team1color, ChatColor team2color, int minPlayers, int maxPlayers, int reqFlags) {
        super(name, maxPlayers, mainSpawn);
        this.minPlayers = minPlayers;
        this.team1spawn = team1spawn;
        this.team2spawn = team2spawn;
        this.team1flag = team1flag;
        this.team2flag = team2flag;
        this.team1color = team1color;
        this.team2color = team2color;
        this.reqFlags = reqFlags;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public Location getTeam1spawn() {
        return team1spawn;
    }

    public Location getTeam2spawn() {
        return team2spawn;
    }

    public ChatColor getTeam1color() {
        return team1color;
    }

    public ChatColor getTeam2color() {
        return team2color;
    }

    public int getReqFlags() {
        return reqFlags;
    }

    public Location getTeam1flag() {
        return team1flag;
    }

    public Location getTeam2flag() {
        return team2flag;
    }
}
