package com.rast.ctf;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class RespawnTimer {

    private int time;
    private final Runnable runnable;
    private final Player gamePlayer;
    private BukkitTask respawnCounter;

    public RespawnTimer(Player player, int time, Runnable runnable) {
        this.time = time;
        this.runnable = runnable;
        gamePlayer = player;
        runTask();
    }

    private void runTask() {
        respawnCounter = Bukkit.getScheduler().runTaskTimer(CTF.getPlugin(), () -> {
            if (time == 0) {
                runnable.run();
                stopTask();
                return;
            } else if (time == 1) {
                gamePlayer.sendTitle(ChatColor.RED + "You died!", ChatColor.GRAY.toString() + "Respawning in " + time + ChatColor.GRAY + " second", 0, 20, 5);
            } else {
                gamePlayer.sendTitle(ChatColor.RED + "You died!", ChatColor.GRAY.toString() + "Respawning in " + time + ChatColor.GRAY + " seconds", 0, 20, 5);
            }
            time--;
        }, 0, 20);
    }

    public void stopTask() {
        respawnCounter.cancel();
    }
}
