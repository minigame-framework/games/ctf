package com.rast.ctf;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Commands implements CommandExecutor, TabCompleter {

    private final List<String> subcommands = new ArrayList<>(Arrays.asList("setkit", "getkit", "kitlist", "delkit"));

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please select a subcommand!");
            return true;
        }

        switch (args[0]) {
            case "setkit":
                if (args.length < 3) {
                    sender.sendMessage(ChatColor.RED + "This subcommand requires two arguments. Kit name and kit logo item.");
                    break;
                }
                try {
                    Material mat = Material.getMaterial(args[2].split(":")[1].toUpperCase());
                    if (mat == null) {
                        sender.sendMessage(ChatColor.RED + "The material " + args[2] + " could not be found!");
                        return true;
                    }
                    CTF.getKits().deleteKit(args[1]);
                    CTF.getKits().saveKit(args[1], ((Player) sender).getInventory(), mat);
                    sender.sendMessage(ChatColor.GOLD + "Kit " + args[1] + " has been saved.");
                } catch (IOException e) {
                    sender.sendMessage(ChatColor.GOLD + "File error occurred, kit could not be saved.");
                }
                break;
            case "getkit":
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.RED + "This subcommand requires a kit name.");
                    break;
                }
                sender.sendMessage(ChatColor.GOLD + "Kit " + args[1] + " has been equipped.");
                ((Player) sender).getInventory().setContents(CTF.getKits().loadKit(args[1]).getContents());
                break;
            case "kitlist":
                sender.sendMessage(ChatColor.GOLD + "List of kits:");
                StringBuilder builder = new StringBuilder(ChatColor.YELLOW.toString());
                for (String kitName : CTF.getKits().kitList()) {
                    builder.append(kitName);
                    builder.append(" | ");
                }
                sender.sendMessage(builder.toString());
                break;
            case "delkit":
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.RED + "This subcommand requires a kit name.");
                    break;
                }
                try {
                    CTF.getKits().deleteKit(args[1]);
                    sender.sendMessage(ChatColor.GOLD + "Kit " + args[1] + " has been deleted.");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        List<String> tabList = new ArrayList<>();

        if (!(sender instanceof Player)) {
            return tabList;
        }

        if (args.length == 1) {
            return subcommands;
        }

        switch (args[0]) {
            case "setkit":
                if (args.length < 3) {
                    break;
                }
                for (Material material : Material.values()) {
                    tabList.add(material.getKey().toString());
                }
                break;
            case "getkit":
            case "delkit":
                tabList.addAll(CTF.getKits().kitList());
                break;
        }
        return tabList;
    }
}
