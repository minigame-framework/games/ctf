package com.rast.ctf;

import com.rast.gamecore.MapConfig;
import com.rast.gamecore.util.ColorText;
import com.rast.gamecore.util.ConfigSettings;
import com.rast.gamecore.util.Region;
import com.rast.gamecore.util.StringLocation;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Settings extends ConfigSettings {

    // Settings variables
    private String localChatFormat;
    private HashMap<String, CTFMapConfig> mapConfigs;
    private boolean autoTNT;
    private long worldDespawnTime, gameCountdown, gameCountdownFast;

    public Settings() {
        CTF.getPlugin().saveDefaultConfig(); // save the default config before getting data
        reload(); // grab some fresh data from the config
    }

    public void reload() {
        // Get the plugin, reload config, and get the config
        CTF plugin = CTF.getPlugin();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        localChatFormat = ColorText.TranslateChat(Objects.requireNonNull(config.getString("local-chat-format")));
        worldDespawnTime = config.getLong("world-despawn-time");
        gameCountdown = config.getLong("game-countdown");
        gameCountdownFast = config.getLong("game-countdown-fast");

        // get the map configs
        mapConfigs = new HashMap<>();
        ConfigurationSection maps = config.getConfigurationSection("maps");
        assert maps != null;
        for (int i = 0; i < maps.getInt("map-count"); i++) {
            ConfigurationSection map = maps.getConfigurationSection("map-" + (i + 1));
            if (map != null) {
                CTFMapConfig mapConfig = new CTFMapConfig(
                        map.getString("name"),
                        StringLocation.toLocation(Objects.requireNonNull(map.getString("main-spawn"))),
                        StringLocation.toLocation(Objects.requireNonNull(map.getString("team-1-spawn"))),
                        StringLocation.toLocation(Objects.requireNonNull(map.getString("team-2-spawn"))),
                        StringLocation.toLocation(Objects.requireNonNull(map.getString("team-1-flag"))),
                        StringLocation.toLocation(Objects.requireNonNull(map.getString("team-2-flag"))),
                        ChatColor.valueOf(Objects.requireNonNull(map.getString("team-1-color"))),
                        ChatColor.valueOf(Objects.requireNonNull(map.getString("team-2-color"))),
                        map.getInt("min-players"),
                        map.getInt("max-players"),
                        map.getInt("required-flags"));
                mapConfigs.put(mapConfig.getName(), mapConfig);
            }
        }
        // end getting map configs

        autoTNT = config.getBoolean("auto-tnt");
    }

    public String getLocalChatFormat() {
        return localChatFormat;
    }

    public boolean doAutoTNT() {
        return autoTNT;
    }

    public CTFMapConfig getMapConfig(String map) {
        return mapConfigs.get(map);
    }

    public List<MapConfig> getMapConfigs() {
        return new ArrayList<>(mapConfigs.values());
    }

    public long getWorldDespawnTime() {
        return worldDespawnTime;
    }

    public long getGameCountdown() {
        return gameCountdown;
    }

    public long getGameCountdownFast() {
        return gameCountdownFast;
    }

    private List<Region> loadRegions(ConfigurationSection map) {
        int regionCount = map.getInt("respawn-region-count");
        List<Region> regionList = new ArrayList<>();
        for (int i = 0; i < regionCount; i++) {
            ConfigurationSection regionConfig = map.getConfigurationSection("respawn-region-" + (i+1));
            if (regionConfig != null) {
                Region tmpRegion = new Region();
                tmpRegion.setRegion(
                        StringLocation.toLocation(Objects.requireNonNull(regionConfig.getString("pos-1"))),
                        StringLocation.toLocation(Objects.requireNonNull(regionConfig.getString("pos-2"))));
                regionList.add(tmpRegion);
            }
        }
        return regionList;
    }
}
